const query = require('./modelos/query');
const deletar = require('./controles/deleta_banco');
const selecionar = require('./controles/select_banco');
const gravar = require('./controles/grava_arquivo')
const busca_arquivos = require('./modelos/busca_arquivos.js')

Fluxo();
async function Fluxo(){
    //Copia o arquivo atual e realiza bkp
    await Buscar()
    
    //deleta dados das tabelas do banco
    await Deletar(query.delete_operacoes,'IFR2_OPERACOES');
    //await Deletar(query.delete_category,'CATEGORY');

    //Grava dados do arquivo CSV
    insert = query.insert_operacoes;
    await Gravar("./arquivos/IFR2.csv","IFR2_OPERACOES",insert)
    //await Tratar("./arquivos/IFR2.csv","CATEGORY")    
        
    //Seleciona dados inseridos nas tabelas do banco
    await Selecionar(query.select_operacoes,'IFR2_OPERACOES');    
    //await Selecionar(query.select_category,'CATEGORY');
    
}

async function Selecionar (query,tabela) {
    await selecionar.select_banco(query, tabela);    
};

async function Deletar (query,tabela) {
    await deletar.deleta_banco(query,tabela);
};

async function Gravar(arquivo,nome_arquivo,insert){
    await gravar.Grava_Arquivo(arquivo,nome_arquivo,insert);
};

async function Buscar(){
    await busca_arquivos.Busca_arquivos();
}
