const pool = require('../conexao/db.js');

function deleta_banco(comando,tabela){
  
  return new Promise(function resolvePromise(resolve,Reject){
    setTimeout(()=>{

        pool.connect((erro, client, done) => {
          if (erro) throw erro

            console.log(``)
            console.log(`DELETANDO DADOS DA TABELA ${tabela}...`)
            console.time('TEMPO TOTAL PARA DELETAR');   

            client.query(comando, (erro, resposta) => {                          
              done()            
              console.timeEnd('TEMPO TOTAL PARA DELETAR');   
              if (erro) {
                console.log(erro.stack)
              } else {
              //console.log(`DELETANDO DADOS DA TABELA ${tabela}...`)
              }
            })
        })

      return resolve({  
        
        })
      },500);

  })
  
}

module.exports = {deleta_banco}