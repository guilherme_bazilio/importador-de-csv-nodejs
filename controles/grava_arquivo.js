const readline = require('readline') //npm install readline
const fs = require('fs')
const pool = require('../conexao/db.js');

async function Grava_Arquivo(Arquivo,Nome_arquivo,insert)
{
    return new Promise(function resolvePromise(resolve,Reject){
    setTimeout(()=>{

    console.log(``)
    console.log('TRATANDO E INSERINDO DADOS DO ARQUIVO:', Nome_arquivo,' ...');   
    console.time('TEMPO TOTAL PARA TRATAR E INSERIR');     

    const arquivo = fs.createReadStream(Arquivo)        
    const linha = readline.createInterface({
        input:arquivo,
    })

    linha.on('line',(line)=>{        
        const texto = line.toUpperCase()
        let resultado = texto.replace(/"R/gi,"");        
        resultado = resultado.replace(/[.*"%+?^${}()|[\]\\-]/g, ""); //remove caracteres especiais         
        resultado = resultado.replace(/[/]]/g, "-"); //remove A BARRA        
        resultado = resultado.replace(/,00000/gi," ");    
        resultado = resultado.replace(/,0000/gi," ");    
        resultado = resultado.replace(/,000/gi," ");    
        resultado = resultado.replace(/,00/gi," ");    
        resultado = resultado.replace(/,/gi,".");    
        resultado = resultado.replace(/;/gi,","); 
        resultado = resultado.replace(/, /gi,",");    
        resultado = resultado.replace(/ ,/gi,",");
        resultado = resultado.replace(/[/]/gi,"-"); // para data        
        resultado = resultado.replace(/,R /gi,",");                  
        resultado = resultado.replace(/, /gi,",0");                
        //console.log('linha completa: ',resultado)        
        
        const linhaSplit = resultado.split(",");
        if (linhaSplit[0]!=="ID"){ // DEIXA DE LADO O CABECALHO
            //console.log('com split:',linhaSplit);
            pool.connect((erro, client, done) => {        
                if (erro) throw erro;
      
                try {
                        client.query(insert, linhaSplit, (erro, resposta) => {                     
                        if (erro) {
                            console.log(erro.stack);
                        } else {
                            console.log(`Inserção do arquivo ${Nome_arquivo}, conteúdo da linha inserida: ${resultado}`)
                        }
                    });

                }catch (error){
                console.log('ALGO DEU ERRADO NA INSERÇÃO',error)
                }finally {                                           
                done();            
                }
            });           
        }        
    })
        return resolve({             
        })
    },500);//Tentativa de promise

  })

}

module.exports= {Grava_Arquivo}

