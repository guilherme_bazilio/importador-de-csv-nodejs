const fs = require('fs');
const bkp = require('fs-extra'); //npm install fs-extra

async function Busca_arquivos(){    
    let data = new Date();
    let dia = data.getDate();
    var mes     = data.getMonth();
    var ano    = data.getFullYear();
    var hora    = data.getHours();          // 0-23
    var min     = data.getMinutes();        // 0-59
    var seg     = data.getSeconds();        // 0-59
    var mseg    = data.getMilliseconds();   // 0-999

    let arquivo_origem = 'D:/3_Bill/Financeiro/Orçamento/2021/Rotina de envio/IFR2.csv';
    let arquivo_destino = 'C:/1 - GIT/3 - Fontes/2 - NodeJS/Importador de CSV/importador-de-csv-nodejs/arquivos/IFR2.csv';
    let arquivo_bkp = 'D:/3_Bill/Financeiro/Orçamento/2021/Rotina de envio/BKP/IFR2'+dia+mes+ano+hora+min+seg+mseg+'.csv';

    await Cria_bkp(arquivo_destino,arquivo_bkp);
    await Copia_arquivo(arquivo_origem,arquivo_destino)
    
}

async function Cria_bkp(Arquivo_destino, Arquivo_bkp){
    try{
        console.log(``)        
        console.log('REALIZANDO O BKP ...'); 
        console.time('TEMPO TOTAL PARA REALIZAR O BKP');           
        await bkp.move(Arquivo_destino, Arquivo_bkp);
        console.timeEnd('TEMPO TOTAL PARA REALIZAR O BKP');           
    }catch(error){
        console.log('DEU RUIM NO BKP',error)
    }    
}

async function Copia_arquivo(Arquivo_origem, Arquivo_destino){
    try{
        console.log(``)        
        console.log('REALIZANDO A CÓPIA ...'); 
        console.time('TEMPO TOTAL PARA REALIZAR A CÓPIA');         
        fs.createReadStream(Arquivo_origem).pipe(fs.createWriteStream(Arquivo_destino));
        console.timeEnd('TEMPO TOTAL PARA REALIZAR A CÓPIA');         
    }catch(error){
        console.log('DEU RUIM NA COPIA',error)
    }    
}

module.exports = {Busca_arquivos}