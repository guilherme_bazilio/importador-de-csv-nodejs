const insert_category =
"INSERT INTO category (id, name, description, created_at) VALUES ($1, $2, $3, $4)";

const insert_operacoes = 
"INSERT INTO IFR2_OPERACOES(id,papel,descricao_papel,valor_aplicado,valor_fechamento,data_aplicacao,data_fechamento,percentual_lucro,valor_lucro)  VALUES ($1, $2, $3,$4,$5,TO_DATE($6,'DD/MM/YYYY'),TO_DATE($7,'DD/MM/YYYY'), $8, $9)";

const delete_operacoes = 
"DELETE FROM IFR2_OPERACOES";

const delete_category = 
"DELETE FROM category";

const select_category =
"SELECT * FROM category";

const select_operacoes = 
"SELECT * FROM IFR2_OPERACOES"

module.exports = {insert_category,insert_operacoes,delete_operacoes,delete_category,select_category,select_operacoes};
